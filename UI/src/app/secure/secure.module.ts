import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TemplateComponent } from './template/template.component';
import { AddComponent } from './add/add.component';
import { OverviewComponent } from './overview/overview.component';
import { SummaryComponent } from './summary/summary.component';
import { SettingComponent } from './setting/setting.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(
      [
        {
          path: '',
          component: TemplateComponent,
          children:[
            {
              path:'overview',
              data: { type:'overview' },
              component : OverviewComponent
            },
            {
              path:'add',
              data: { type:'add' },
              component : AddComponent
            },
            {
              path:'summary',
              data: { type:'summary' },
              component : SummaryComponent
            },
            {
              path:'setting',
              data: { type:'setting' },
              component : SettingComponent
            },
            { path: '**',
              redirectTo: 'overview'
            }
          ]
        }
      ]
    )
  ],
  declarations: [
    TemplateComponent, 
    AddComponent, 
    OverviewComponent, 
    SummaryComponent, SettingComponent
  ],
    exports: [
      RouterModule
    ]
})
export class SecureModule { }
