import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.scss']
})
export class TemplateComponent implements OnInit {
 
  constructor(
    public activeroute:ActivatedRoute
  ) { }
 public title:string = "Welcome";

  ngOnInit() {
    this.activeroute.data.subscribe(
      (data)=>{
        console.log(data);
        if(data.type == 'Overview'){
          this.title = 'overview';
        } 
      }
    )
  }

}
