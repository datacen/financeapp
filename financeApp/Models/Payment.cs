﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace financeApp.Models
{
    public class Payment
    {
        public int PaymentID {  get;set;  }
        
        public int UserID { get; set; }
        public int CategoryID { get; set; }

        public string Title { get; set; }
        public string Type { get; set; }
        public int Amount { get; set; }
        public string Description { get; set; }
        
        public DateTime DateCreated { get; set; }

        public virtual User User { get; set; }
        public virtual Category Category { get; set; }
    }
}