﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace financeApp.Models
{
    public class User
    {
        public int UserID { get; set; }

        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string EMail { get; set; }

        public virtual ICollection<Payment> Payments { get; set; }
    }
}