﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace financeApp.Models
{
    public class Category
    {
        public int CategoryID { get; set; }

        public string Category_Name { get; set; }

        public virtual ICollection<Payment> Payments { get; set; }
    }
}